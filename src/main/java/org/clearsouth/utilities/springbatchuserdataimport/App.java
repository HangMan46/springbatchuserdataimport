package org.clearsouth.utilities.springbatchuserdataimport;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
	public static void main(String[] args) {

		String[] springConfig  = {
                "spring/batch/config/context.xml",
                "spring/batch/config/database.xml",
				"spring/batch/jobs/job-report.xml"
			};
		
		ApplicationContext context = 
				new ClassPathXmlApplicationContext(springConfig);
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job tTYPES0Job = (Job) context.getBean("tTYPES0Job");

		try {

			JobExecution execution = jobLauncher.run(tTYPES0Job, new JobParameters());
			System.out.println("tTYPES0Job Exit Status : " + execution.getStatus());

		} catch (Exception e) {
			e.printStackTrace();
		}

        Job tPROFILE0Job = (Job) context.getBean("tPROFILE0Job");

        try {

            JobExecution execution = jobLauncher.run(tPROFILE0Job, new JobParameters());
            System.out.println("tPROFILE0Job Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Job tTYPES1Job = (Job) context.getBean("tTYPES1Job");

        try {

            JobExecution execution = jobLauncher.run(tTYPES1Job, new JobParameters());
            System.out.println("tTYPES1Job Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Job tPROFILE1Job = (Job) context.getBean("tPROFILE1Job");

        try {

            JobExecution execution = jobLauncher.run(tPROFILE1Job, new JobParameters());
            System.out.println("tPROFILE1Job Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Job tTYPES2Job = (Job) context.getBean("tTYPES2Job");

        try {

            JobExecution execution = jobLauncher.run(tTYPES2Job, new JobParameters());
            System.out.println("tTYPES2Job Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Job tPROFILE2Job = (Job) context.getBean("tPROFILE2Job");

        try {

            JobExecution execution = jobLauncher.run(tPROFILE2Job, new JobParameters());
            System.out.println("tPROFILE2Job Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Job tUSERJob = (Job) context.getBean("tUSERJob");

        try {

            JobExecution execution = jobLauncher.run(tUSERJob, new JobParameters());
            System.out.println("tUSERJob Exit Status : " + execution.getStatus());

        } catch (Exception e) {
            e.printStackTrace();
        }

		System.out.println("Done");

	}
}
